'use strict'

{
    class App {
        constructor(element) {
            this.element = element;
            this.init();
        }

        init() {
            this.attachEvents();
        }

        attachEvents () {
            let createCardButton = this.element.querySelector("#createCardButton");

            createCardButton.addEventListener('click', event => {
                event.preventDefault();
                let titleInput = this.element.querySelector("#titleInput");
                let descriptionInput = this.element.querySelector("#descriptionInput");
                let importanceSelect = this.element.querySelector("#importanceSelect");

                let cardData = {
                    title: titleInput.value,
                    description: descriptionInput.value,
                    importance: importanceSelect.value
                }

                new Card(cardData);
            })
        }

    }

    class Card {
        constructor(cardData) {
            console.log(cardData);
            this.cardData = cardData;
            this.cardImportance = cardData.importance;
            this.cardsBlock = document.querySelector("#cardsBlock");
            this.init();
        }

        createCard(cardData) {
            let card = document.createElement('div');
            card.classList.add('card');


            let cardHTML = `<div class="card-body">
                            <span class="badge ${this.importanceClass}">${cardData.importance}</span>
                            <h5 class="card-title">${cardData.title}</h5>
                            <p class="card-text">${cardData.description}</p>
                            <a href="#" class="btn btn-primary complete-button">Complete</a>
                            <a href="#" class="btn btn-info edit-button">Edit</a>
                            <a href="#" class="btn btn-danger delete-button">Delete</a>
                            </div>`
            card.innerHTML = cardHTML;
            console.log(this.cardsBlock);

            this.cardsBlock.append(card);
        }

        get importanceClass()  {
            switch (this.cardImportance) {
                case 'High':
                    return 'badge-danger'
                case 'Medium':
                    return 'badge-warning'
                case 'Low':
                    return 'badge-success'
            }

        }

        init() {
            this.createCard(this.cardData);
        }
    }

    let appElement = document.querySelector("#app");

    new App(appElement);

}